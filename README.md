# Practica7
* Tomar como base la práctica anterior
* Hacer un despliegue con Jenkins o GitLab Auto DevOps donde considere las fases de:
* Prueba unitaria (o de sistema)
* Revisión de estándares de código
* Revisión de calidad de código (análisis)
* Construcción de artefactos
* Entrega de los artefactos en un repositorio
* El último commit debe estar atado a un log GitLab o Jenkins donde se muestre el éxito de todos los anteriores.
* Incluir en el readme el acceso al repositorio de artefactos, donde pueda descargarse el último.

## Requerimientos
* instalar nodejs
* instalar sonarqube
* instalar jenkins

## video

https://drive.google.com/drive/folders/1YjMYI3hwBiS9zc5VnuzKZqdUVaQPOu8U?usp=sharing

## Autora

* Andrea Lissette Flores Aguilar 201314241